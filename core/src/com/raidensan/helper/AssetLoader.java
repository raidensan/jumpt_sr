package com.raidensan.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.raidensan.gameworld.Variables;

import java.util.Iterator;

/**
 * Created by Fersad on 28.08.2015.
 */
public class AssetLoader {

    public static Texture texture,heroTexture,bg,coin,acid,cutter,logoTexture, highScoreBG, uiElements,creditsTexture,transportCircleTexture,libGDXLogoTexture;
    public static TextureRegion platform1, platform2, platform3;
    public static TextureRegion hero1,hero2,hero3,hero4,hero5,hero6;
    public static TextureRegion heroFalling, heroJumping;
    public static TextureRegion coin1,coin2,coin3,coin4,coin5,coin6,coin7,coin8,coin9;
    public static TextureRegion cutter1,cutter2,cutter3,cutter4,cutter5,cutter6,cutter7,cutter8,cutter9;
    public static TextureRegion bg1;
    public static TextureRegion acidRegion;
    public static TextureRegion logo,libGDXLogo;
    public static TextureRegion playButtonUp, playButtonDown;
    public static TextureRegion soundEnabledUp, soundEnabledDown,soundDisabledUp,soundDisabledDown,infoButtonUp,infoButtonDown,creditsRegion;
    public static TextureRegion tc1,tc2,tc3,tc4,tc5,tc6,tc7,tc8,tc9;

    public static BitmapFont font,shadow,altFont,titleFont,titleFontShadow,subTitleFont, subTitleFontShadow,textFont,textFontShadow;

    public static Sound deadOnCutter,grunt, jump,land, energyCell,acidSplash;

    public static Animation heroAnimation, coinAnimation, cutterAnimation,tcAnimation;

    public static Preferences prefs;

    public static void load(){

        prefs = Gdx.app.getPreferences("ZombieBird");

        // Provide default high score of 0
        if (!prefs.contains("highScore")) {
            prefs.putInteger("highScore", 0);
        }

        // Provide default high score of 0
        if (!prefs.contains("playSound")) {
            prefs.putBoolean("playSound", true);
        }

        texture = new Texture(Gdx.files.internal("data/platform.png"));
        heroTexture = new Texture(Gdx.files.internal("data/hero_sprite.png"));
        bg = new Texture(Gdx.files.internal("data/bg.png"));
        highScoreBG = new Texture(Gdx.files.internal("data/highscore.png"));
        highScoreBG.setFilter(TextureFilter.Linear,TextureFilter.Linear );

        libGDXLogoTexture = new Texture(Gdx.files.internal("data/libgdx.png"));
        libGDXLogoTexture.setFilter(TextureFilter.Linear,TextureFilter.Linear);
        libGDXLogo = new TextureRegion(libGDXLogoTexture,0,0,300,50);
        libGDXLogo.flip(false,true);

        transportCircleTexture = new Texture(Gdx.files.internal("data/transport_circle.png"));
        transportCircleTexture.setFilter(TextureFilter.Linear,TextureFilter.Linear);

        acid = new Texture(Gdx.files.internal("data/acid_sea.png"));
        acidRegion = new TextureRegion(acid,0,0,916,544);
        acidRegion.flip(false, false);
        acid.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        coin =new Texture(Gdx.files.internal("data/energy_sprites.png"));
        cutter = new Texture(Gdx.files.internal("data/cutter_sprites.png"));

        logoTexture = new Texture(Gdx.files.internal("data/logo.png"));

        uiElements = new Texture(Gdx.files.internal("data/ui_sprites.png"));
        uiElements.setFilter(TextureFilter.Linear,TextureFilter.Linear);



        deadOnCutter = Gdx.audio.newSound(Gdx.files.internal("data/male_agony_scream.wav"));
        jump = Gdx.audio.newSound(Gdx.files.internal("data/rocket.wav"));
        land = Gdx.audio.newSound(Gdx.files.internal("data/land.wav"));
        grunt  = Gdx.audio.newSound(Gdx.files.internal("data/male_grunt_.wav"));
        acidSplash = Gdx.audio.newSound(Gdx.files.internal("data/acid-splash.wav"));
        energyCell = Gdx.audio.newSound(Gdx.files.internal("data/coin_collect.wav"));

        logoTexture.setFilter(TextureFilter.Linear,TextureFilter.Linear);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        heroTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        bg.setFilter(TextureFilter.Linear,TextureFilter.Linear);
        coin.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        cutter.setFilter(TextureFilter.Linear,TextureFilter.Linear);

        playButtonUp = new TextureRegion(uiElements, 1539, 0,512,512);
        playButtonDown = new TextureRegion(uiElements, 1539, 0, 512,512);
        playButtonUp.flip(false, true);
        playButtonDown.flip(false, true);

        soundDisabledDown = new TextureRegion(uiElements, 513, 0,513,513);
        soundDisabledUp = new TextureRegion(uiElements, 513, 0, 513,513);
        soundDisabledDown.flip(false, true);
        soundDisabledUp.flip(false, true);

        soundEnabledDown = new TextureRegion(uiElements, 0, 0,513,513);
        soundEnabledUp = new TextureRegion(uiElements, 0, 0, 513,513);
        soundEnabledDown.flip(false, true);
        soundEnabledUp.flip(false, true);

        infoButtonDown = new TextureRegion(uiElements, 1026, 0,513,513);
        infoButtonUp = new TextureRegion(uiElements, 1026, 0, 513,513);
        infoButtonDown.flip(false, true);
        infoButtonUp.flip(false, true);

        creditsTexture = new Texture(Gdx.files.internal("data/credits.png"));
        creditsRegion = new TextureRegion(creditsTexture,0,0,446,288);
        creditsRegion.flip(false,true);

        logo = new TextureRegion(logoTexture,916, 544);

        bg1= new TextureRegion(bg,256,400,1041,240);
        bg1.flip(false, true);


        heroFalling = new TextureRegion(heroTexture,257,257,256,257);
        heroFalling.flip(false, true);

        heroJumping = new TextureRegion(heroTexture,257,0,256,257);
        heroJumping.flip(false, true);



        coin1 = new TextureRegion(coin,0,0,256,256);
        coin1.flip(false,true);
        coin2 = new TextureRegion(coin,0,256,256,256);
        coin2.flip(false,true);
        coin3 = new TextureRegion(coin,0,512,256,256);
        coin3.flip(false,true);
        coin4 = new TextureRegion(coin,0,768,256,256);
        coin4.flip(false,true);
        coin5 = new TextureRegion(coin,0,1024,256,256);
        coin5.flip(false,true);
        coin6 = new TextureRegion(coin,0,1280,256,256);
        coin6.flip(false,true);
        coin7 = new TextureRegion(coin,0,1536,256,256);
        coin7.flip(false,true);
        coin8 = new TextureRegion(coin,0,1792,256,256);
        coin8.flip(false,true);
        coin9 = new TextureRegion(coin,0,2048,256,256);
        coin9.flip(false,true);

        //        cutter 86*42

        cutter1 = new TextureRegion(cutter,0,0,256,128);
        cutter1.flip(false,true);
        cutter2 = new TextureRegion(cutter,0,256,256,128);
        cutter2.flip(false,true);
        cutter3 = new TextureRegion(cutter,0,512,256,128);
        cutter3.flip(false,true);
        cutter4 = new TextureRegion(cutter,0,768,256,128);
        cutter4.flip(false,true);
        cutter5 = new TextureRegion(cutter,0,1024,256,128);
        cutter5.flip(false,true);
        cutter6 = new TextureRegion(cutter,0,1280,256,128);
        cutter6.flip(false,true);
        cutter7 = new TextureRegion(cutter,0,1536,256,128);
        cutter7.flip(false,true);
        cutter8 = new TextureRegion(cutter,0,1792,256,128);
        cutter8.flip(false,true);
        cutter9 = new TextureRegion(cutter,0,2048,256,128);
        cutter9.flip(false,true);

        // Hero animation
        hero1 = new TextureRegion(heroTexture, 0,514,257, 256);
        hero1.flip(false, true);

        hero2 = new TextureRegion(heroTexture,0,770,257,256);
        hero2.flip(false, true);

        hero3 = new TextureRegion(heroTexture,0,0,257,257);
        hero3.flip(false,true);

        hero4 = new TextureRegion(heroTexture,0,257,257,257);
        hero4.flip(false,true);

        hero5 = new TextureRegion(heroTexture,257,514,256,256);
        hero5.flip(false, true);

        hero6 = new TextureRegion(heroTexture,257,770,256,256);
        hero6.flip(false, true);

        platform1 = new TextureRegion(texture,0,0,256,32);
        platform1.flip(false, true);

        // Transport Circle
        tc1 = new TextureRegion(transportCircleTexture,0,0,256,256);
        tc2 = new TextureRegion(transportCircleTexture,0,256,256,256);
        tc3 = new TextureRegion(transportCircleTexture,0,512,256,256);
        tc4 = new TextureRegion(transportCircleTexture,0,768,256,256);
        tc5 = new TextureRegion(transportCircleTexture,0,1024,256,256);
        tc6 =new TextureRegion(transportCircleTexture,0,1280,256,256);
        tc7 = new TextureRegion(transportCircleTexture,0,1536,256,256);
        tc8 = new TextureRegion(transportCircleTexture,0,1792,256,256);
        tc9 = new TextureRegion(transportCircleTexture,0,2048,256,256);

        TextureRegion[] circles = {tc1,tc2,tc3,tc4,tc5,tc6,tc7,tc8,tc9};
        for(int i = 0 ; i <=8 ; i++ ){
            circles[i].flip(false,true);
        }

        tcAnimation = new Animation(0.1f,circles);
        tcAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        TextureRegion[] heroes = { hero1,hero2,hero3,hero4,hero5,hero6 };
        heroAnimation = new Animation(0.1f, heroes);
        heroAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        TextureRegion[] coins = {coin1,coin2,coin3,coin4,coin5,coin6,coin7,coin8,coin9};
        coinAnimation = new Animation(0.1f,coins);
        coinAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        
        TextureRegion[] cutters = {cutter1,cutter2,cutter3,cutter4,cutter5,cutter6,cutter7,cutter8,cutter9};
        cutterAnimation = new Animation(0.1f,cutters);
        cutterAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        platform2 = platform1;
        platform3 = platform1;

        font = new BitmapFont(Gdx.files.internal("data/sovjet.fnt"));
        font.getData().setScale(0.25f,-0.25f);
        shadow = new BitmapFont(Gdx.files.internal("data/sovjet_shadow.fnt"));
        shadow.getData().setScale(0.25f,-0.25f);

        titleFont = new BitmapFont(Gdx.files.internal("data/sovjet.fnt"));
        titleFont.getData().setScale(0.5f,-0.5f);
        titleFontShadow = new BitmapFont(Gdx.files.internal("data/sovjet_shadow.fnt"));
        titleFontShadow.getData().setScale(0.5f,-0.5f);

        subTitleFont = new BitmapFont(Gdx.files.internal("data/sovjet.fnt"));
        subTitleFont.getData().setScale(0.35f,-0.35f);
        subTitleFontShadow = new BitmapFont(Gdx.files.internal("data/sovjet_shadow.fnt"));
        subTitleFontShadow.getData().setScale(0.35f,-0.35f);

        textFont=new BitmapFont(Gdx.files.internal("data/sovjet.fnt"));
        textFont.getData().setScale(0.20f,-0.20f);
        textFontShadow=new BitmapFont(Gdx.files.internal("data/sovjet_shadow.fnt"));
        textFontShadow.getData().setScale(0.20f,-0.20f);

        altFont = new BitmapFont(Gdx.files.internal("data/sovjet.fnt"));
        altFont.getData().setScale(0.15f,-0.2f);

    }

    public static void dispose(){
        texture.dispose();
        heroTexture.dispose();
        coin.dispose();
        bg.dispose();
        font.dispose();
        shadow.dispose();
        acid.dispose();
        logoTexture.dispose();
        deadOnCutter.dispose();
        jump.dispose();
        grunt.dispose();
        energyCell.dispose();
        land.dispose();

    }

    // Receives an integer and maps it to the String highScore in prefs
    public static void setHighScore(int val) {
        prefs.putInteger("highScore", val);
        prefs.flush();
    }

    // Retrieves the current high score
    public static int getHighScore() {
        return prefs.getInteger("highScore");
    }

    public static void setPlaySound(boolean val) {
        prefs.putBoolean("playSound", val);
        prefs.flush();
    }

    // Retrieves the current high score
    public static boolean getPlaySound() {
        return prefs.getBoolean("playSound");
    }
}
