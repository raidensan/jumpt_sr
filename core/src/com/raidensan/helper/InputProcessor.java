package com.raidensan.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.raidensan.gameobjects.Hero;
import com.raidensan.gameworld.GameWorld;
import com.raidensan.gameworld.Variables;
import com.raidensan.ui.SimpleButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fersad on 28.08.2015.
 */
public class InputProcessor implements com.badlogic.gdx.InputProcessor {

    private Hero myHero;
    private GameWorld myWorld;

    private List<SimpleButton> menuButtons;
    private SimpleButton playButton,soundButton,creditsButton;

    private float scaleFactorX;
    private float scaleFactorY;

    public InputProcessor(GameWorld myWorld, float scaleFactorX,
                          float scaleFactorY){

        Gdx.input.setCatchBackKey(true);

        this.myWorld = myWorld;
        myHero=myWorld.getHero();

        int midPointY = myWorld.getMidPointY();

        this.scaleFactorX = scaleFactorX;
        this.scaleFactorY = scaleFactorY;

        menuButtons = new ArrayList<SimpleButton>();
        playButton = new SimpleButton(
                204 / 2 - (78/ 2),
                136/4, 78, 50f, AssetLoader.playButtonUp,
                AssetLoader.playButtonDown);

        if (AssetLoader.getPlaySound()==true) {
            soundButton = new SimpleButton(
                    204 / 4 - (22.5f/ 2),
                    ((136/4)*3)-30, 22.5f, 22.5f, AssetLoader.soundEnabledUp,
                    AssetLoader.soundEnabledUp);

        }
        else {
            soundButton = new SimpleButton(
                    204/4 - (22.5f/ 2),
                    ((136/4)*3)-30, 22.5f, 22.5f, AssetLoader.soundDisabledUp,
                    AssetLoader.soundDisabledUp);
        }

        // TODO set position and sprite of credits button
        creditsButton = new SimpleButton(((204 / 4)*3) - (22.5f/ 2),
                ((136/4)*3)-30, 22.5f, 22.5f, AssetLoader.infoButtonUp,
                AssetLoader.infoButtonDown);

        menuButtons.add(playButton);
        menuButtons.add(soundButton);
        menuButtons.add(creditsButton);
    }

    @Override
    public boolean keyDown(int keycode) {
        // TODO intercept BACK key and navigate to Main menu
        if (keycode == Input.Keys.BACK || keycode == Input.Keys.SPACE) {
            if (!myWorld.isMenu())
                myWorld.menu();
            else
                Gdx.app.exit();

        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        if (myWorld.isMenu()){
            if(playButton.isTouchDown(screenX,screenY))
                myWorld.restart();
            if (soundButton.isTouchDown(screenX,screenY))
                return true;

            if (creditsButton.isTouchDown(screenX,screenY))
                myWorld.credits();
        }
        else if (myWorld.isReady()){
            myWorld.start();
        }
        else if (myWorld.isCredits()){
            myWorld.menu();
        }
        else if(myWorld.isRunning())
            myHero.onClick();
        else if (myWorld.isGameOver() || myWorld.isHighScore()){
            myWorld.restart();
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        if (myWorld.isMenu()){
            if(playButton.isTouchUp(screenX,screenY)){
                myWorld.ready();
                return true;
            }
            else if (soundButton.isTouchUp(screenX,screenY)){
                if (AssetLoader.getPlaySound()==false){
                    AssetLoader.setPlaySound(true);
                    soundButton.setTouchUpImage(AssetLoader.soundEnabledUp);
                }
                else {
                    AssetLoader.setPlaySound(false);
                    soundButton.setTouchUpImage(AssetLoader.soundDisabledUp);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private int scaleX(int screenX) {
        return (int) (screenX / scaleFactorX);
    }

    private int scaleY(int screenY) {
        return (int) (screenY / scaleFactorY);
    }

    public List<SimpleButton> getMenuButtons(){
        return menuButtons;
    }
}
