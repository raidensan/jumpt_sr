package com.raidensan.jumpt;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.raidensan.helper.AssetLoader;
import com.raidensan.screens.GameScreen;
import com.raidensan.screens.SplashScreen;

public class Jumpt extends Game {

	@Override
	public void create() {
		AssetLoader.load();

// TODO implement high Score!

		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose(){
		super.dispose();
		AssetLoader.dispose();
	}
}
