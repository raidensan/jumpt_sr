package com.raidensan.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.raidensan.gameobjects.AcidSea;
import com.raidensan.gameobjects.Background;
import com.raidensan.gameobjects.Coin;
import com.raidensan.gameobjects.Cutter;
import com.raidensan.gameobjects.Hero;
import com.raidensan.gameobjects.Platform;
import com.raidensan.gameobjects.ScrollHandler;
import com.raidensan.helper.AssetLoader;
import com.raidensan.helper.InputProcessor;
import com.raidensan.tweenaccessors.Value;
import com.raidensan.tweenaccessors.ValueAccessor;
import com.raidensan.ui.SimpleButton;

import java.util.List;
import java.util.Random;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * Created by Fersad on 27.08.2015.
 */
public class GameRenderer {

    private GameWorld myWorld;
    private OrthographicCamera cam;
    private ShapeRenderer shapeRenderer;

    private SpriteBatch batcher;

    private ScrollHandler scroller;
    private Background bgObj;
    private AcidSea acidSea;
    private Platform platform1,platform2,platform3;
    private Hero hero;
    private Coin coin1,coin2,coin3;
    private TextureRegion p1,p2,p3;
    private TextureRegion bg;

    private TextureRegion heroFalling,heroJumping;

    private Texture bgTexture,acidTexture;
    private TextureRegion bgRegion,acidRegion;
    private Animation heroAnimation,coinAnimation,cutterAnimation;

    private TweenManager manager;
    private Value alpha = new Value();

    private List<SimpleButton> menuButtons;

    private int midPointY;
    private int gameHeight;

    Texture tex, tex2;

    ShaderProgram blurShader;
    FrameBuffer blurTargetA, blurTargetB;
    TextureRegion fboRegion;

    public static final int FBO_SIZE = 204;
    public static final int FBO_SIZE1 = 136;

    public static final float MAX_BLUR = 2f;

    BitmapFont fps;

    final String VERT =
            "attribute vec4 "+ShaderProgram.POSITION_ATTRIBUTE+";\n" +
                    "attribute vec4 "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
                    "attribute vec2 "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +

                    "uniform mat4 u_projTrans;\n" +
                    " \n" +
                    "varying vec4 vColor;\n" +
                    "varying vec2 vTexCoord;\n" +

                    "void main() {\n" +
                    "	vColor = "+ShaderProgram.COLOR_ATTRIBUTE+";\n" +
                    "	vTexCoord = "+ShaderProgram.TEXCOORD_ATTRIBUTE+"0;\n" +
                    "	gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" +
                    "}";

    final String FRAG =
            "#ifdef GL_ES\n" +
                    "#define LOWP lowp\n" +
                    "precision mediump float;\n" +
                    "#else\n" +
                    "#define LOWP \n" +
                    "#endif\n" +
                    "varying LOWP vec4 vColor;\n" +
                    "varying vec2 vTexCoord;\n" +
                    "\n" +
                    "uniform sampler2D u_texture;\n" +
                    "uniform float resolution;\n" +
                    "uniform float radius;\n" +
                    "uniform vec2 dir;\n" +
                    "\n" +
                    "void main() {\n" +
                    "	vec4 sum = vec4(0.0);\n" +
                    "	vec2 tc = vTexCoord;\n" +
                    "	float blur = radius/resolution; \n" +
                    "    \n" +
                    "    float hstep = dir.x;\n" +
                    "    float vstep = dir.y;\n" +
                    "    \n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y - 4.0*blur*vstep)) * 0.05;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y - 3.0*blur*vstep)) * 0.09;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y - 2.0*blur*vstep)) * 0.12;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y - 1.0*blur*vstep)) * 0.15;\n" +
                    "	\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x, tc.y)) * 0.16;\n" +
                    "	\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 1.0*blur*hstep, tc.y + 1.0*blur*vstep)) * 0.15;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 2.0*blur*hstep, tc.y + 2.0*blur*vstep)) * 0.12;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 3.0*blur*hstep, tc.y + 3.0*blur*vstep)) * 0.09;\n" +
                    "	sum += texture2D(u_texture, vec2(tc.x + 4.0*blur*hstep, tc.y + 4.0*blur*vstep)) * 0.05;\n" +
                    "\n" +
                    "	gl_FragColor = vColor * vec4(sum.rgb, 1.0);\n" +
                    "}";

    public GameRenderer(GameWorld world, int gameHeight, int midPointY){
        myWorld=world;

        this.menuButtons = ((InputProcessor) Gdx.input.getInputProcessor()).getMenuButtons();

        this.midPointY=midPointY;
        this.gameHeight=gameHeight;



        cam=new OrthographicCamera();
        cam.setToOrtho(true, 204, 136);
        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        initAssets();
        initGameObjects();
        setupTweens();

        prepareShader();

    }

    private void prepareShader() {

        //tex = new Texture();

        tex = bgTexture;
        tex2 = acidTexture;
        tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        tex2.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        //important since we aren't using some uniforms and attributes that SpriteBatch expects
        ShaderProgram.pedantic = false;

        blurShader = new ShaderProgram(VERT, FRAG);
        if (!blurShader.isCompiled()) {
            System.err.println(blurShader.getLog());
            System.exit(0);
        }
        if (blurShader.getLog().length()!=0)
            System.out.println(blurShader.getLog());

        //setup uniforms for our shader
        blurShader.begin();
        blurShader.setUniformf("dir", 0f, 0f);
        blurShader.setUniformf("resolution", FBO_SIZE);
        blurShader.setUniformf("radius", 1f);
        blurShader.end();

        blurTargetA = new FrameBuffer(Pixmap.Format.RGBA8888, FBO_SIZE, FBO_SIZE1, false);
        blurTargetB = new FrameBuffer(Pixmap.Format.RGBA8888, FBO_SIZE, FBO_SIZE1, false);
        fboRegion = new TextureRegion(blurTargetA.getColorBufferTexture());
        fboRegion.flip(true, true);

        //batcher = new SpriteBatch();

        fps = new BitmapFont();
    }

    private void resizeBatch(int width, int height) {
        cam.setToOrtho(true, width, height);
        batcher.setProjectionMatrix(cam.combined);
    }

    private void renderEntities(SpriteBatch batch) {
        batch.draw(bgRegion, 0, 0,Variables.ScreenWidth / 2, Variables.ScreenHeight / 2);
        batch.draw(acidRegion,0,0,Variables.ScreenWidth / 2, Variables.ScreenHeight / 2);
    }

    private void renderBlur(){
        blurTargetA.begin();

        //Clear the offscreen buffer with an opaque background
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //before rendering, ensure we are using the default shader
        batcher.setShader(null);

        //resize the batch projection matrix before drawing with it
        resizeBatch(204, 136);

        //now we can start drawing...
        //batcher.begin();

        //draw our scene here
        renderEntities(batcher);

        //finish rendering to the offscreen buffer
        batcher.flush();

        //finish rendering to the offscreen buffer
        blurTargetA.end();

        //now let's start blurring the offscreen image
        batcher.setShader(blurShader);

        //since we never called batch.end(), we should still be drawing
        //which means are blurShader should now be in use

        //ensure the direction is along the X-axis only
        blurShader.setUniformf("dir", 1f, 0f);

        //update blur amount based on touch input
        float mouseXAmt = Gdx.input.getX() / (float)Gdx.graphics.getWidth();
        blurShader.setUniformf("radius", mouseXAmt * MAX_BLUR);

        //our first blur pass goes to target B
        blurTargetB.begin();

        //we want to render FBO target A into target B
        fboRegion.setTexture(blurTargetA.getColorBufferTexture());
        //fboRegion.flip(true,true);

        //draw the scene to target B with a horizontal blur effect
        batcher.draw(fboRegion, 0, 0);

        //flush the batch before ending the FBO
        batcher.flush();

        //finish rendering target B
        blurTargetB.end();

        //now we can render to the screen using the vertical blur shader

        //update our projection matrix with the screen size
        resizeBatch(204, 136);

        //update the blur only along Y-axis
        blurShader.setUniformf("dir", 0f, 1f);

        //update the Y-axis blur radius
        float mouseYAmt = Gdx.input.getY() / (float)Gdx.graphics.getHeight();
        blurShader.setUniformf("radius", mouseYAmt * MAX_BLUR);

        //draw target B to the screen with a vertical blur effect
        fboRegion.setTexture(blurTargetB.getColorBufferTexture());
        //fboRegion.flip(true,true);
        batcher.draw(fboRegion, 0, 0);

        //reset to default shader without blurs
        batcher.setShader(null);

        //draw FPS
        //fps.draw(batcher, String.valueOf(Gdx.graphics.getFramesPerSecond()), 5, Gdx.graphics.getHeight()-5);

        //finally, end the batch since we have reached the end of the frame
        //batcher.end();
    }

    private void setupTweens() {
        Tween.registerAccessor(Value.class, new ValueAccessor());
        manager = new TweenManager();
        Tween.to(alpha, -1, .5f).target(0).ease(TweenEquations.easeOutQuad)
                .start(manager);
    }

    private void initAssets() {
        p1 = AssetLoader.platform1;
        p2 = AssetLoader.platform2;
        p3 = AssetLoader.platform3;
        heroAnimation = AssetLoader.heroAnimation;
        coinAnimation = AssetLoader.coinAnimation;
        bg = AssetLoader.bg1;
        heroFalling = AssetLoader.heroFalling;
        heroJumping = AssetLoader.heroJumping;
        acidTexture = AssetLoader.acid;
        cutterAnimation = AssetLoader.cutterAnimation;
    }

    private void initGameObjects(){
        scroller = myWorld.getScroller();
        hero = myWorld.getHero();
        platform1= scroller.getPlatform1();
        platform2=scroller.getPlatform2();
        platform3=scroller.getPlatform3();
        coin1 = scroller.getCoin1();
        coin2 = scroller.getCoin2();
        coin3 = scroller.getCoin3();
        bgObj = scroller.getBackground();
        acidSea = scroller.getAcidSea();
        bgTexture = new Texture(Gdx.files.internal("data/bg.png"));
        bgTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    }

    private void drawPlatforms(){
        batcher.draw(p1, platform1.getX(), platform1.getY(), platform1.getWidth(), platform1.getHeight());
        batcher.draw(p2, platform2.getX(),platform2.getY(),platform2.getWidth(),platform2.getHeight());
        batcher.draw(p3, platform3.getX(), platform3.getY(), platform3.getWidth(), platform3.getHeight());
    }

    private void drawCoins(float runTime){
        if (!coin1.isScored())
            batcher.draw(coinAnimation.getKeyFrame(runTime),coin1.getX(),coin1.getY(),coin1.getWidth(),coin1.getHeight());
        if (!coin2.isScored())
            batcher.draw(coinAnimation.getKeyFrame(runTime),coin2.getX(),coin2.getY(),coin2.getWidth(),coin2.getHeight());
        if (!coin3.isScored())
            batcher.draw(coinAnimation.getKeyFrame(runTime),coin3.getX(),coin3.getY(),coin3.getWidth(),coin3.getHeight());
    }

    public void bgCreate(){
        bgRegion= new TextureRegion(bgTexture, -1 * (int)bgObj.getX(),0,408,272);
        bgRegion.flip(false, true);
    }

    public void acidCreate(){
        acidRegion= new TextureRegion(acidTexture, -1 * (int)acidSea.getX(),0,816,544);
        acidRegion.flip(false, true);
    }

    public void render(float runTime,float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        bgCreate();
        acidCreate();

        batcher.begin();

        batcher.disableBlending();

        bg.setRegionX(-1 * (int) bgObj.getX());
        batcher.draw(bgRegion, 0, 0, Variables.ScreenWidth / 2, Variables.ScreenHeight / 2);

        batcher.enableBlending();

        drawPlatforms();

        drawCoins(runTime);

        drawCutter(runTime);

        if (!hero.isAlive()){
            // fadeout implementation
            Color oldColor = batcher.getColor();
            float oldAlpha = oldColor.a;

            float alphaScale = 0.95f;

            oldColor.a = alphaScale * myWorld.getFadeoutAlpha();
            myWorld.setFadeoutAlpha(alphaScale * myWorld.getFadeoutAlpha());

            batcher.setColor(oldColor);

            batcher.draw(heroAnimation.getKeyFrame(runTime), hero.getX(), hero.getY(), hero.getWidth(), hero.getHeight());
            batcher.draw(AssetLoader.tcAnimation.getKeyFrame(runTime), hero.getX() + 2, hero.getY() + 7, hero.getWidth(), hero.getHeight());

            oldColor.a = oldAlpha;
            batcher.setColor(oldColor);
        }
        else if (hero.isFalling()){
            batcher.draw(heroFalling, hero.getX(), hero.getY(), hero.getWidth(), hero.getHeight());
        }
        else if (hero.isJumping()) {
            batcher.draw(heroJumping, hero.getX(), hero.getY(), hero.getWidth(), hero.getHeight());
        }
        else {
            batcher.draw(heroAnimation.getKeyFrame(runTime), hero.getX(), hero.getY(), hero.getWidth(), hero.getHeight());
        }


        String touchMessage = "TAP TO START";
        String gameOverMessage = "YOU DIED!";
        String tryAgainMessage = "TAP TO TRY AGAIN";

        if (myWorld.isReady()) {
            // Draw shadow first
            AssetLoader.titleFontShadow.draw(batcher,touchMessage, (Variables.ScreenWidth / 4) - (5 *touchMessage.length()), 66);
            // Draw text
            AssetLoader.titleFont.draw(batcher, touchMessage, (Variables.ScreenWidth / 4)  - ((5*touchMessage.length()) - 1), 65);
        } else if (myWorld.isGameOver()  || myWorld.isHighScore()) {
            //batcher.draw(AssetLoader.highScoreBG,0,0,204,136);

            AssetLoader.subTitleFontShadow.draw(batcher, gameOverMessage, (Variables.ScreenWidth / 4) - ((4 * gameOverMessage.length())), 36);
            AssetLoader.subTitleFont.draw(batcher, gameOverMessage, (Variables.ScreenWidth / 4) -((4 * gameOverMessage.length()) - 1), 35);

            String score = "SCORE : " + myWorld.getScore() + "";
            AssetLoader.subTitleFontShadow.draw(batcher, "" + score, (Variables.ScreenWidth / 4) -((4 * score.length())), 55);
            AssetLoader.subTitleFont.draw(batcher, "" + score, (Variables.ScreenWidth / 4)-((4 * score.length())-1), 54);



            if (myWorld.isHighScore()){
                String highScore = "HIGH SCORE!";

                AssetLoader.subTitleFontShadow.draw(batcher, highScore, (Variables.ScreenWidth / 4) -((4 * highScore.length())), 76);
                AssetLoader.subTitleFont.draw(batcher, highScore, (Variables.ScreenWidth / 4)-((4 * highScore.length())-1), 75);
            }

            AssetLoader.subTitleFontShadow.draw(batcher, tryAgainMessage, (Variables.ScreenWidth / 4) -((4 * tryAgainMessage.length())), 96);
            AssetLoader.subTitleFont.draw(batcher, tryAgainMessage, (Variables.ScreenWidth / 4)-((4 * tryAgainMessage.length())-1), 95);

        }else if (myWorld.isRunning()) {
            String jumpsLeft = "JUMPS LEFT: " + (3 - hero.getJumpCount()) + "";
            AssetLoader.shadow.draw(batcher, jumpsLeft, 1, 5);
            AssetLoader.font.draw(batcher, jumpsLeft, 1, 4);
        } else if (myWorld.isMenu()){
            drawMenuUI();
        }
        else if (myWorld.isCredits()){

            renderBlur();
            drawCredits();
        }

        if (myWorld.getScore() > 0 && myWorld.isRunning()) {
            String score = "SCORE : " + myWorld.getScore() + "";
            AssetLoader.shadow.draw(batcher, "" + score, 155, 5);
            AssetLoader.font.draw(batcher, "" + score, 154, 4);
        }



        if (!myWorld.isCredits()) {
            acidRegion.setRegionX(-1 * (int) acidSea.getX());
            batcher.draw(acidRegion, -2, 5, Variables.ScreenWidth / 2 + 2, Variables.ScreenHeight / 2);
        }
//        drawWarningText();
        batcher.end();
        drawTransition(delta);
//        drawBoundingShapes();
    }

    private void drawCutter(float runTime) {
        if (platform1.getCutter().isEnabled()) {
            batcher.draw(cutterAnimation.getKeyFrame(runTime), platform1.getCutter().getX(), platform1.getCutter().getY(), platform1.getCutter().getWidth(), platform1.getCutter().getHeight());
        }

        if (platform2.getCutter().isEnabled()) {
            batcher.draw(cutterAnimation.getKeyFrame(runTime), platform2.getCutter().getX(), platform2.getCutter().getY(), platform2.getCutter().getWidth(), platform2.getCutter().getHeight());
        }

        if (platform3.getCutter().isEnabled()) {
            batcher.draw(cutterAnimation.getKeyFrame(runTime), platform3.getCutter().getX(), platform3.getCutter().getY(), platform3.getCutter().getWidth(), platform3.getCutter().getHeight());
        }
    }

    private void drawBoundingShapes(){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(hero.getBoundingRect().x, hero.getBoundingRect().y, hero.getBoundingRect().width, hero.getBoundingRect().height);
//        shapeRenderer.circle(coin1.getBoundingCircle().x, coin1.getBoundingCircle().y, coin1.getBoundingCircle().radius);
//        shapeRenderer.circle(coin2.getBoundingCircle().x,coin2.getBoundingCircle().y,coin2.getBoundingCircle().radius);
//        shapeRenderer.circle(coin3.getBoundingCircle().x,coin3.getBoundingCircle().y,coin3.getBoundingCircle().radius);
        shapeRenderer.setColor(Color.CYAN);
        shapeRenderer.rect(platform1.getBoundingRect().x, platform1.getBoundingRect().y, platform1.getBoundingRect().width, platform1.getBoundingRect().height);
        shapeRenderer.rect(platform2.getBoundingRect().x,platform2.getBoundingRect().y,platform2.getBoundingRect().width,platform2.getBoundingRect().height);
        shapeRenderer.rect(platform3.getBoundingRect().x,platform3.getBoundingRect().y,platform3.getBoundingRect().width,platform3.getBoundingRect().height);

        shapeRenderer.circle(platform1.getCutter().getBoundingCircle().x, platform1.getCutter().getBoundingCircle().y, platform1.getCutter().getBoundingCircle().radius);
        shapeRenderer.circle(platform2.getCutter().getBoundingCircle().x,platform2.getCutter().getBoundingCircle().y,platform2.getCutter().getBoundingCircle().radius);
        shapeRenderer.circle(platform3.getCutter().getBoundingCircle().x,platform3.getCutter().getBoundingCircle().y,platform3.getCutter().getBoundingCircle().radius);

        shapeRenderer.end();
    }

    private void drawWarningText(){
        AssetLoader.altFont.draw(batcher, "Testing Release, not Playable. raidensan.com", 5, 125);
    }

    private void drawMenuUI(){
        //batcher.setColor(0,0,0,0.1f);
        for(SimpleButton button : menuButtons){
            button.draw(batcher);
        }
    }

    private void drawCredits(){

        String gameTitle,author,authorText,graphics,graphicsText1,graphicsText2,graphicsText3,graphicsText4;
        String graphicsText  = "Farshad DELIRABDINIA,Alev BOZDUMAN, irmirx (behance.net/irmirx),pzUH (gameart2d.com),freesound.org & fontlibrary.org";
        gameTitle = "JUMP'T";
        author = "Design & Development";
        authorText = "Farshad DELIRABDINIA";
        graphics=" Graphics & Assets";

        String[] texts = graphicsText.split(",");

        for (int i = 0; i < texts.length;i++){
            AssetLoader.textFontShadow.draw(batcher,texts[i],(Variables.ScreenWidth/4) - (texts[i].length() * 2f)-1,72-1  +(10 * i));
            AssetLoader.textFont.draw(batcher,texts[i],(Variables.ScreenWidth/4) - (texts[i].length() * 2f),72  +(10 * i));
        }

        AssetLoader.titleFontShadow.draw(batcher,gameTitle, (Variables.ScreenWidth/4) - (gameTitle.length() * 5f)-1,10-1);
        AssetLoader.titleFont.draw(batcher,gameTitle, (Variables.ScreenWidth/4) - (gameTitle.length() * 5f),10);
        AssetLoader.subTitleFontShadow.draw(batcher,author,(Variables.ScreenWidth/4) - (author.length() * 3.5f)-1,30-1);
        AssetLoader.subTitleFont.draw(batcher,author,(Variables.ScreenWidth/4) - (author.length() * 3.5f),30);
        AssetLoader.textFontShadow.draw(batcher,authorText,(Variables.ScreenWidth/4) - (authorText.length() * 2f)-1,45-1);
        AssetLoader.textFont.draw(batcher,authorText,(Variables.ScreenWidth/4) - (authorText.length() * 2f),45);

        AssetLoader.subTitleFontShadow.draw(batcher,graphics,(Variables.ScreenWidth/4) - (graphics.length() * 3.5f)-1,60-1);
        AssetLoader.subTitleFont.draw(batcher,graphics,(Variables.ScreenWidth/4) - (graphics.length() * 3.5f),60);


        batcher.draw(AssetLoader.libGDXLogo,(Variables.ScreenWidth/4) - 15,125,30,5);
    }

    private void drawTransition(float delta) {
        if (alpha.getValue() > 0) {
            manager.update(delta);
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(1, 1, 1, alpha.getValue());
            shapeRenderer.rect(0, 0, 204, 136);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);

        }
    }
}
