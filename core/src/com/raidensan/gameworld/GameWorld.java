package com.raidensan.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.raidensan.gameobjects.Coin;
import com.raidensan.gameobjects.Hero;
import com.raidensan.gameobjects.ScrollHandler;
import com.raidensan.helper.AssetLoader;

/**
 * Created by Fersad on 27.08.2015.
 */
public class GameWorld {

    private ScrollHandler scroller;
    private Hero hero;

    private boolean isPlayingSound = false;
    private boolean isGameStarted = false;

    private int midPointY;

    private int score = 0;

    private float fadeoutAlpha,fadeinAlpha ;

    private GameState currentState;

    public void credits() {
        currentState=GameState.CREDITS;
    }

    public boolean isCredits() {
        return currentState == GameState.CREDITS;
    }

    public void menu() {
        currentState=GameState.MENU;
    }

    public enum GameState {

        MENU, READY, RUNNING, GAMEOVER, HIGHSCORE, CREDITS

    }

    public GameWorld(int midPointY){
        fadeoutAlpha = 1.0f;
        fadeinAlpha = 0.1f;
        currentState = GameState.MENU;
        this.midPointY = midPointY;
        hero = new Hero(40,0,36,36);
        scroller = new ScrollHandler(this,midPointY);


        if (AssetLoader.getPlaySound()) {
//            AssetLoader.bgMusic.loop(0.1f);
            isPlayingSound=true;
        }

    }

    public int getScore(){
        return score;
    }

    public void addScore(int increment){
        score += increment;
    }

    public void updateRunning(float delta,float runtime){

        if (delta > .15f) {
            delta = .15f;
        }

        if (AssetLoader.getPlaySound()==true)
            if (!isPlayingSound) {
//                AssetLoader.bgMusic.loop(1.0f);
                isPlayingSound=true;
            }
        else if (isPlayingSound && AssetLoader.getPlaySound()==false) {
                isPlayingSound=false;
            }


        if ((runtime % 10.0f > 0.9f) && (runtime % 10.0f < 1.0f)  && hero.isAlive()){
            int newSpeed = (scroller.getCurrentSpeed() - 5);
            if (!scroller.speedChanged()) {
                //scroller.setSpeed(newSpeed);
                //scroller.setSpeedChanged(true);
            }
        }
        else {
            scroller.setSpeedChanged(false);
        }

        hero.update(delta);
        scroller.update(delta);

        if (scroller.platformCollidesSide(hero)) {

            if (hero.isAlive()) {
                if (AssetLoader.getPlaySound()) {
                    if (!hero.isFallingToAcid())
                        AssetLoader.grunt.play();
                }

                hero.setIsPlatform(true);
                scroller.stop();
                hero.fall();
                if (hero.isHitGround()) {
                    hero.die();
                    currentState = GameState.GAMEOVER;
                    if (score > AssetLoader.getHighScore()) {
                        AssetLoader.setHighScore(score);
                        currentState = GameState.HIGHSCORE;
                    }
                }
                AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.NORMAL);
            }
        }
        else if (scroller.platformCollides(hero)) {
            if (hero.isAlive()) {
                hero.setIsPlatform(false);
                hero.setIsAcid(false);
                hero.setIsCut(false);
                hero.walk(scroller.getCollidingPlatform().getY());
                AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
            }
        }
        else if (scroller.getPlatform1().getCutter().collides(hero) || scroller.getPlatform2().getCutter().collides(hero) || scroller.getPlatform3().getCutter().collides(hero)){

            if (AssetLoader.getPlaySound()) {
                AssetLoader.deadOnCutter.play();
            }

            scroller.stop();
            hero.die();
            hero.decelerate();
            hero.setIsCut(true);
            currentState=GameState.GAMEOVER;

            AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.NORMAL);

            if (score > AssetLoader.getHighScore()) {
                AssetLoader.setHighScore(score);
                currentState = GameState.HIGHSCORE;
            }
        }
        else if (scroller.coinColides(hero)){
            scroller.getCollidingCoin().setScored(true);

            if (AssetLoader.getPlaySound())
                AssetLoader.energyCell.play();

            AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        } else if (hero.isHitGround()){
            if (AssetLoader.getPlaySound()) {
                AssetLoader.acidSplash.play();
                AssetLoader.deadOnCutter.play();
            }

            scroller.stop();
            hero.die();
            hero.decelerate();
            hero.setIsAcid(true);
            currentState=GameState.GAMEOVER;
            AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.NORMAL);
            if (score > AssetLoader.getHighScore()) {
                AssetLoader.setHighScore(score);
                currentState = GameState.HIGHSCORE;
            }
        }

    }

    public void update(float delta,float runtime){

//        Gdx.app.log("GameWorld","currentState: " + currentState + "");

        switch (currentState) {
            case MENU:
            case READY:
                isGameStarted = false;
                updateReady(delta);

                break;
            case RUNNING:
                isGameStarted = true;
                updateRunning(delta,runtime);

                break;
            case GAMEOVER:
                isGameStarted = false;
                updateRunning(delta,runtime);
                break;
            case CREDITS:
                isGameStarted = false;
                updateReady(delta);
                break;
            default:
                break;
        }
    }

    private void updateReady(float delta){

        if (AssetLoader.getPlaySound()==true && !isPlayingSound) {
            isPlayingSound=true;
        }
        else if (isPlayingSound && AssetLoader.getPlaySound()==false) {
            isPlayingSound=false;
        }

        AssetLoader.heroAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        hero.updateReady(0);
        scroller.updateReady(delta);
    }

    public ScrollHandler getScroller(){
        return scroller;
    }

    public Hero getHero(){
        return hero;
    }

    public void start() { currentState = GameState.RUNNING; }

    public void ready() {
        currentState = GameState.READY;
    }

    public void restart() {
        fadeinAlpha = 0.1f;
        fadeoutAlpha = 1.0f;
        score = 0;
        hero.onRestart(0);
        scroller.onRestart();
        currentState = GameState.READY;
    }

    public boolean isReady() {
        return currentState == GameState.READY;
    }

    public boolean isGameOver() {
        return currentState == GameState.GAMEOVER;
    }

    public boolean isHighScore() {
        return currentState == GameState.HIGHSCORE;
    }

    public boolean isMenu() { return currentState == GameState.MENU; }

    public boolean isRunning() {
        return currentState == GameState.RUNNING;
    }

    public int getMidPointY(){return  midPointY;}

    public float getFadeoutAlpha(){ return  fadeoutAlpha; }

    public  void setFadeoutAlpha(float newAlpha) {
        if (newAlpha <= 0.0f)
            fadeoutAlpha=0.0f;
        else if (newAlpha >= 1.0f)
            fadeoutAlpha =1.0f;
        else
            fadeoutAlpha = newAlpha;
    }

    public float getFadeinAlpha() {return  fadeinAlpha;}

    public void setFadeinAlpha(float newAlpha){
        if (newAlpha>1.0f)
            fadeinAlpha = 1.0f;
        else if (newAlpha<0.0f)
            fadeinAlpha = 0.0f;
        else
            fadeinAlpha = newAlpha;
    }



}
