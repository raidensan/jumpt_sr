package com.raidensan.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Fersad on 23.09.2015.
 */
public class SimpleButton {

    private  float x,y,width,height;

    private TextureRegion buttonUp,buttonDown;

    private Rectangle boundingRect;

    private boolean isPressed = false;

    public SimpleButton(float x, float y, float width, float height, TextureRegion buttonUp, TextureRegion buttonDown){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.buttonUp= buttonUp;
        this.buttonDown = buttonDown;

        boundingRect = new Rectangle(x,y,width,height);
    }

    public  boolean isClicked(int screenX, int screenY){
        return boundingRect.contains(screenX,screenY);
    }

    public  void draw(SpriteBatch batcher){
        if(isPressed)
            batcher.draw(buttonDown,x,y,width,height);
        else
            batcher.draw(buttonUp,x,y,width,height);
    }

    public boolean isTouchDown(int screenX,int screenY){
        if (boundingRect.contains(screenX,screenY)){
            isPressed = true;
            return true;
        }
        return  false;
    }

    public boolean isTouchUp(int screenX,int screenY){
        if (boundingRect.contains(screenX,screenY) && isPressed){
            isPressed = false;
            return true;
        }
        return false;
    }

    public void setTouchUpImage(TextureRegion reg){
        this.buttonUp = reg;
    }

    public void setTouchDownImage(TextureRegion reg){
        this.buttonDown = reg;
    }
}
