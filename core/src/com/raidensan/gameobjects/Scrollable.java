package com.raidensan.gameobjects;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Fersad on 28.08.2015.
 */
public class Scrollable {
    protected Vector2 position;
    protected Vector2 velocity;
    protected int width;
    protected int height;
    protected boolean isScrolledLeft;

    public Scrollable(float x, float y, int w, int h, float scrollSpeed){
        position = new Vector2(x,y);
        velocity =  new Vector2(scrollSpeed,0);
        this.width=w;
        this.height=h;
        isScrolledLeft=false;
    }

    public void update(float delta){
        position.add(velocity.cpy().scl(delta));

        if(position.x + width < 0){
            isScrolledLeft=true;    // Object out of screen
        }
    }

    public void reset(float newX){
        position.x=newX;
        isScrolledLeft=false;
    }

    public void reset(float newX,float newY){
        position.x=newX;
        position.y = newY;
        isScrolledLeft=false;
    }

    public void setScrollSpeed(int scrollSpeed){
        velocity.x = scrollSpeed;
    }

    public void stop() {
        velocity.x = 0;
    }

    public boolean isOffScreen(){
        return isScrolledLeft;
    }

    public float getTailX(){
        return position.x+width;
    }

    public float getX(){
        return position.x;
    }

    public float getY(){
        return position.y;
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }



}
