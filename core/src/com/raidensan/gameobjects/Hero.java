package com.raidensan.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.raidensan.gameworld.Variables;
import com.raidensan.helper.AssetLoader;

/**
 * Created by Fersad on 27.08.2015.
 */
public class Hero {
    private Vector2 position;
    private Vector2 velocity;
    private Vector2 acceleration;

    private float rotation;

    private int width;
    private int height;

    private boolean isAlive;
    private boolean isWalking;

    private boolean isCut;
    private boolean isAcid;
    private boolean isPlatform;

    private int jumpCount;

    private Rectangle boundingRect;
    private boolean fallingToAcid =false;

    public Hero(float x, float y,int w, int h){
        this.width=w;
        this.height=h;
        position=new Vector2(x,y);
        velocity=new Vector2(0,0);
        acceleration = new Vector2(0,460);
        boundingRect = new Rectangle();
        rotation = 0;
        isAlive=true;
        isAcid=false;
        isCut=false;
        isPlatform=false;
        isWalking = false;
        jumpCount = 0;

    }


    public void die(){
        velocity.y=0;
        isAlive = false;
    }

    public boolean isAlive(){
        return isAlive;
    }

    public void decelerate(){
        acceleration.y = 0;
    }

    public void update(float delta){
        velocity.add(acceleration.cpy().scl(delta));

        if (velocity.y>200){
            velocity.y=200;
        }

        if (position.y < -25) {
            position.y = -25;
            velocity.y = 0;
        }


        position.add(velocity.cpy().scl(delta));

        boundingRect.set(position.x + (24-(17/2)), position.y +8, width - 32, height-8);

    }

    public void updateReady(float runTime){
        position.y = -100;
    }

    public Rectangle getBoundingRect(){
        return boundingRect;
    }

    private boolean isJumping = false;

    public void onClick(){
        if (isAlive) {
            if (jumpCount<3){
                isWalking=false;
                jumpCount++;
                velocity.y = -150;
                if (AssetLoader.getPlaySound())
                    AssetLoader.jump.play(0.5f);
                isJumping = true;
            }
        }
    }

    public void onRestart(int y) {
        position.y = y;
        velocity.x = 0;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = 460;
        isAlive = true;
        isAcid=false;
        isCut=false;
        isPlatform=false;
        fallingToAcid=false;
        jumpCount=0;
    }

    public boolean isFalling() {
        return velocity.y > 30;
    }

    public boolean isJumping() {return isJumping;}

    public boolean shouldntRun() {
        return velocity.y > 70;
    }

    public void walk(float newY){
        if (isAlive) {
            if (!isWalking) {
                if (AssetLoader.getPlaySound())
                    AssetLoader.land.play(0.3f);
            }
            isJumping = false;
            velocity.y = -20 ; //-20
            jumpCount=0;
            isWalking=true;
        }
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public boolean isDead(){
        return this.position.y + height > Variables.ScreenHeight /2;
    }

    public boolean isHitGround() {
        if (isAlive)
            return position.y + height > (Variables.ScreenHeight / 2);

        return false;
    }

    public float getRotation(){ return  rotation;}

    public boolean getIsCut(){ return  isCut;}

    public boolean getIsAcid() {return  isAcid;}

    public void setIsCut(boolean state) { isCut = state;}

    public  void setIsAcid(boolean state) {isAcid = state;}

    public boolean getIsPlatform(){return  isPlatform;}

    public void setIsPlatform(boolean state) { isPlatform = state;}

    public int getJumpCount(){return jumpCount;}

    public void fall() {
        velocity.y = 100;
        fallingToAcid = true;
    }

    public boolean isFallingToAcid(){
        return fallingToAcid;
    }
}
