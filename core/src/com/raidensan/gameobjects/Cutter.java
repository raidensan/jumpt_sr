package com.raidensan.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

/**
 * Created by Fersad on 20.09.2015.
 */
public class Cutter extends Scrollable {

    private Random r;

    private boolean enabled;

    private Rectangle boundingRect;

    private Circle boundingCircle;

    public Cutter(float x, float y, int w, int h, float scrollSpeed) {
        super(x, y, w, h, scrollSpeed);
        r = new Random();
        boundingRect = new Rectangle();
        boundingCircle = new Circle();
        isScored = false;
        enabled = false;
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        isScored = false;
    }

    @Override
    public void reset(float newX,float newY){
        super.reset(newX,newY);
        isScored=false;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        boundingRect.set(position.x, position.y, width, height);
        boundingCircle.set(position.x + 8, position.y + 8, (width / 2)-3);
    }

    public Rectangle getBoundingRect() {
        return boundingRect;
    }

    public Circle getBoundingCircle() {
        return boundingCircle;
    }

    public boolean collides(Hero hero) {
        if (!enabled)
            return false;
        if (hero.isAlive()) {
            if (position.y < hero.getY() + hero.getHeight()) {
                return Intersector.overlaps(boundingCircle, hero.getBoundingRect());
            }
        }
        return false;
    }

    private boolean isScored;

    public void setScored(boolean b) {
        isScored = b;
    }

    public boolean isScored() {
        return isScored;
    }

    public void onRestart(float x, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x);
    }

    public boolean isEnabled(){
        return enabled;
    }

    public void setEnabled(boolean isEnabled){
        enabled=isEnabled;
    }

    public void setPosition(float newX,float newY){
        position.x = newX;
        position.y=newY;
    }
}
