package com.raidensan.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by Fersad on 31.08.2015.
 */
public class SpeedBooster extends Scrollable {

    private Random r;

    private Rectangle boundingRect;

    private Circle boundingCircle;

    public SpeedBooster(float x, float y, int w, int h, float scrollSpeed) {
        super(x, y, w, h, scrollSpeed);
        r=new Random();
        boundingRect = new Rectangle();
        boundingCircle = new Circle();
        isScored=false;
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        position.y = r.nextInt(50);
        isScored=false;
    }

    @Override
    public void update(float delta){
        super.update(delta);
        boundingRect.set(position.x, position.y, width, height);
        boundingCircle.set(position.x + 8 ,position.y + 8, (width/2)-3);
    }

    public Rectangle getBoundingRect(){
        return boundingRect;
    }
    public Circle getBoundingCircle(){ return boundingCircle;}

    public boolean collides(Hero hero){

        if (position.y + 5 < hero.getY() + hero.getHeight()) {
            return Intersector.overlaps(boundingCircle, hero.getBoundingRect());
        }

        return false;
    }

    private boolean isScored;

    public void setScored(boolean b){
        isScored = b;
    }

    public boolean isScored(){
        return isScored;
    }

    public void onRestart(float x, float scrollSpeed){
        velocity.x=scrollSpeed;
        reset(x);
    }

}