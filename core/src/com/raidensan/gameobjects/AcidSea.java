package com.raidensan.gameobjects;

/**
 * Created by Fersad on 17.09.2015.
 */
public class AcidSea extends Scrollable {
    public AcidSea(float x, float y, int w, int h, float scrollSpeed){
        super(x,y,w,h,scrollSpeed);
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
//        position.x = 0;
    }

    @Override
    public void update(float delta){
        super.update(delta);
    }

    public void onRestart(float x, float scrollSpeed){
        velocity.x=scrollSpeed;
        reset(x);
    }
}
