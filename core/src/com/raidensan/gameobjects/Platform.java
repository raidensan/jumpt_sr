package com.raidensan.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Random;

/**
 * Created by Fersad on 27.08.2015.
 */
public class Platform extends Scrollable {

    private static final int MIN_PLATFORM_Y = 90;
    private static final int MAX_PLATFORM_Y = 125;

    private Cutter cutter;

    private Random rnd;

    private Rectangle boundingRect;

    public Platform(float x, float y, int w, int h, float scrollSpeed) {
        super(x, y, w, h, scrollSpeed);
        rnd=new Random();
        boundingRect = new Rectangle();
        cutter = new Cutter(this.getX() + (this.getWidth()/2) - (8),(this.getY() - 8),16,8,scrollSpeed);
        cutter.setEnabled(false);
        rnd = new Random();
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        cutter.reset(newX);
//        width = r.nextInt(50) + 10;
        if (rnd.nextInt(50) + 50 > MAX_PLATFORM_Y)
            position.y = MAX_PLATFORM_Y;
        else if (rnd.nextInt(50) + 50 < MIN_PLATFORM_Y)
            position.y = MIN_PLATFORM_Y;
        else
            position.y = rnd.nextInt(50) + 50;

        int R = rnd.nextInt(5-1) + 1;

        if(R==1){
            cutter.setEnabled(true);
        }
        else{
            cutter.setEnabled(false);
        }
    }

    @Override
    public void update(float delta){
        super.update(delta);



        cutter.setPosition(this.getX() + (this.getWidth() / 2) - (8), (this.getY() - 8));

        cutter.update(delta);
        boundingRect.set(position.x,position.y, width,height);
    }

    public Rectangle getBoundingRect(){
        return boundingRect;
    }

    public boolean collides(Hero hero){
        if (position.y <= hero.getY() + hero.getHeight()) {
            Rectangle rect = hero.getBoundingRect();
            return Intersector.overlaps(rect,boundingRect);
        }
        return false;
    }

    public boolean   collidesSide(Hero hero){
        if (hero.isAlive()) {
            float heroYH = hero.getY() + hero.getHeight();
            float heroXW = hero.getX() + hero.getWidth();
            if (heroYH > position.y + 5 && heroXW > position.x + 5) {
                if (Intersector.overlaps(hero.getBoundingRect(), boundingRect)) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public void onRestart(float x, float scrollSpeed){
        velocity.x=scrollSpeed;
        reset(x);
        cutter.reset(this.getX() + (this.getWidth() / 2) - (8),(this.getY() - 8));
    }

    public  Cutter getCutter(){
        return cutter;
    }

//    public void setCutterChance(int chan)
}
