package com.raidensan.gameobjects;

import com.badlogic.gdx.Gdx;
import com.raidensan.gameworld.GameWorld;
import com.raidensan.gameworld.Variables;
import com.raidensan.helper.AssetLoader;

import java.util.Random;

/**
 * Created by Fersad on 28.08.2015.
 */
public class ScrollHandler {

    private Background bg;
    private AcidSea acidSea;
    private Platform platform1, platform2, platform3,collidingPlatform;
    private Coin collidingCoin;
    private Coin coin1,coin2,coin3;
    private Cutter cutter;
    private Random rnd;

    public static final int SCROLL_SPEED=-89; // was -59
    public static final int BG_SCROLL_SPEED=-59;
    public static final int PLATFORM_GAP = 40;
    public static final int COIN_GAP = 200;

    private int currentSpeed = SCROLL_SPEED;

    private boolean speedChanged = false;


    private GameWorld gameWorld;

    public ScrollHandler(GameWorld gameWorld,float yPos){
        this.gameWorld = gameWorld;
        bg = new Background(0,0,1224,272,BG_SCROLL_SPEED);
        acidSea = new AcidSea(0,(Variables.ScreenHeight/2) - 5,816,544,SCROLL_SPEED);

        rnd = new Random();

        int Low = -40;
        int High = 40;
        int R = rnd.nextInt(High-Low) + Low;

        coin1 = new Coin(30,yPos - (2*R),10,10,SCROLL_SPEED );
        R = rnd.nextInt(High-Low) + Low;
        coin2 = new Coin(coin1.getTailX() + COIN_GAP,yPos - (2*R),10,10,SCROLL_SPEED );
        R = rnd.nextInt(High-Low) + Low;
        coin3 = new Coin(coin2.getTailX() + COIN_GAP,yPos - (2*R),10,10,SCROLL_SPEED );


        Low = 80;
        High = 120;

        R = rnd.nextInt(High-Low) + Low;

        platform1 = new Platform(0,R,48,6,SCROLL_SPEED);
        R = rnd.nextInt(High-Low) + Low;
        platform2 = new Platform(platform1.getTailX() + PLATFORM_GAP,R,48,6,SCROLL_SPEED );
        R = rnd.nextInt(High-Low) + Low;
        platform3 = new Platform(platform2.getTailX()+ PLATFORM_GAP, R,48,6,SCROLL_SPEED);

//        cutter = platform1.getCutter();
    }

    private void addScore(int increment) {
        gameWorld.addScore(increment);
    }

    public void update(float delta){

        bg.update(delta);
        acidSea.update(delta);

        platform1.update(delta);
        platform2.update(delta);
        platform3.update(delta);

        coin1.update(delta);
        coin2.update(delta);
        coin3.update(delta);

        if (bg.isOffScreen())
            bg.reset(0.0f);

        if (acidSea.isOffScreen())
            acidSea.reset(0.0f);




        if (coin1.isOffScreen())
            coin1.reset(coin3.getTailX() + COIN_GAP);
        else if (coin2.isOffScreen())
            coin2.reset(coin1.getTailX() + COIN_GAP);
        else if (coin3.isOffScreen())
            coin3.reset(coin2.getTailX() + COIN_GAP);

        if (platform1.isOffScreen()){
            platform1.reset(platform3.getTailX() + PLATFORM_GAP);
        } else if (platform2.isOffScreen()){
            platform2.reset(platform1.getTailX() + PLATFORM_GAP);
        } else if (platform3.isOffScreen()){
            platform3.reset(platform2.getTailX() + PLATFORM_GAP);
        }

    }

    public void updateReady(float delta){
        bg.update(delta);
        acidSea.update(delta);

        if (bg.isOffScreen())
            bg.reset(0.0f);

        if (acidSea.isOffScreen())
            acidSea.reset(0.0f);

    }

    public boolean platformCollides(Hero hero){
        if (platform1.collides(hero)){
            collidingPlatform = platform1;
            return  true;
        } else if (platform2.collides(hero)){
            collidingPlatform = platform2;
            return true;
        }
        else if (platform3.collides(hero)){
            collidingPlatform = platform3;
            return true;
        }
        return false;
//        return (platform1.collides(hero) || platform2.collides(hero) || platform3.collides(hero));
    }

    public boolean platformCollidesSide(Hero hero){
        return (platform1.collidesSide(hero) || platform2.collidesSide(hero) || platform3.collidesSide(hero));
    }

    public void onRestart() {
        bg.onRestart(0, BG_SCROLL_SPEED);
        coin1.onRestart(45,SCROLL_SPEED);
        coin2.onRestart(coin1.getTailX() + COIN_GAP,SCROLL_SPEED);
        coin3.onRestart(coin2.getTailX() + COIN_GAP,SCROLL_SPEED);
        platform1.onRestart(0, SCROLL_SPEED);
        platform2.onRestart(platform1.getTailX() + PLATFORM_GAP, SCROLL_SPEED);
        platform3.onRestart(platform2.getTailX() + PLATFORM_GAP, SCROLL_SPEED);
        acidSea.onRestart(0,SCROLL_SPEED);
        setSpeed(SCROLL_SPEED);

    }

    public boolean coinColides(Hero hero){

        if (!coin1.isScored() && coin1.collides(hero)){

            addScore(1);
            coin1.setScored(true);
            collidingCoin = coin1;
//            setSpeed(SCROLL_SPEED * 2);
            return true;
        }
        else if (!coin2.isScored() && coin2.collides(hero)){

            addScore(1);
            coin2.setScored(true);
            collidingCoin = coin2;
//            setSpeed(SCROLL_SPEED * 2);
            return true;
        }
        else if (!coin3.isScored() && coin3.collides(hero)){

            coin3.setScored(true);
            addScore(1);
            collidingCoin = coin3;
//            setSpeed(SCROLL_SPEED * 2);
            return true;
        }

        return false;
    }

    public Coin getCollidingCoin(){
        return collidingCoin;
    }

    public Platform getCollidingPlatform() {return collidingPlatform; }

    public Platform getPlatform1(){
        return platform1;
    }

    public Platform getPlatform2(){
        return platform2;
    }

    public Platform getPlatform3(){
        return platform3;
    }

    public Coin getCoin1(){return coin1;}
    public Coin getCoin2(){return coin2;}
    public Coin getCoin3(){return coin3;}

    public Background getBackground() { return bg; }

    public AcidSea getAcidSea() {return  acidSea;}

    public Cutter getCutter(){
        return cutter;
    }

    public void stop() {
        platform1.stop();
        platform2.stop();
        platform3.stop();
        coin1.stop();
        coin2.stop();
        coin3.stop();
        bg.stop();
        acidSea.stop();
    }

    public void setSpeed(int newSpeed){
        if (newSpeed < -200)
            return;

        currentSpeed=newSpeed;
        bg.setScrollSpeed((BG_SCROLL_SPEED * newSpeed) / SCROLL_SPEED);
        platform1.setScrollSpeed(newSpeed);
        platform2.setScrollSpeed(newSpeed);
        platform3.setScrollSpeed(newSpeed);
        acidSea.setScrollSpeed(newSpeed);
        coin1.setScrollSpeed(newSpeed);
        coin2.setScrollSpeed(newSpeed);
        coin3.setScrollSpeed(newSpeed);

    }

    public boolean speedChanged(){
        return speedChanged;
    }

    public void setSpeedChanged(boolean state){
        speedChanged = state;
    }

    public int getCurrentSpeed(){
        return currentSpeed;
    }

    public  int getInitialSpeed(){
        return SCROLL_SPEED;
    }

}
