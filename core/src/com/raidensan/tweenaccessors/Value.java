package com.raidensan.tweenaccessors;

/**
 * Created by Fersad on 23.09.2015.
 */
public class Value {
    private float val = 1;

    public float getValue(){
        return val;
    }

    public  void setValue(float newVal){
        val = newVal;
    }
}
