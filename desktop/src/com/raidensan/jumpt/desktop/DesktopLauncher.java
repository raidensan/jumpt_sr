package com.raidensan.jumpt.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.raidensan.jumpt.Jumpt;
import com.raidensan.gameworld.Variables;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "Jump't";
		config.width= Variables.ScreenWidth;
		config.height=Variables.ScreenHeight;



		new LwjglApplication(new Jumpt(), config);
	}
}
